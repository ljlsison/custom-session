console.log("Hello World")


const materials = ['Hydrogen', 'Helium', 'Lithium', 'Beryllium']

console.log(materials.map(material => material.length));

// Single Line Functions
/*
Traditional Function:
	function sampleFunc(a) {
	return a + 100
}*/

sampleFunc1 = (a) => a + 100

console.log(sampleFunc1(100))


// Multi Line Functions

/*
Traditional Function:
	function sampleFunc2(a,b){
	return a + b + 100;
}*/

sampleFunc2 = (a,b) => a + b + 240

console.log(sampleFunc2(100,100))

/*Traditional Function:
	function sampleFunc3 (a,b) {
			const chuck = 42;
			return a + b + chuck;
}
*/

sampleFunc3 = (a,b) => {
	const chuck = 42;
	return a + b + chuck
}

console.log(sampleFunc3(11,23))


// Rest Parameters
function sum(...theArgs) {
  let total = 0;
  for (const arg of theArgs) {
    total += arg;
  }
  return total;
}

console.log(sum(1, 2, 3));
// expected output: 6

console.log(sum(1, 2, 3, 4));
// expected output: 10

// Default Parameters
function multiply(a, b = 1) {
  return a * b;
}

console.log(multiply(5, 2));
// expected output: 10

console.log(multiply(5));
// expected output: 5


// Arrow Functions Used as methods

const obj = {
	i:10,
	b: () => console.log(this.i, this),
	c() {
		console.log(this.i, this);

	}
}

obj.b()
obj.c()


const simple = (a) => a > 15 ? true: false;
console.log(simple(16));
console.log(simple(10));


const arr = [5, 6, 13, 0, 1, 18, 23];

const sum2 = arr.reduce((a,b) => a + b)

console.log(sum2)

const even = arr.filter((c) => c % 2 === 0)

console.log(even)

const double = arr.map((v) => v*2)

console.log(double)
